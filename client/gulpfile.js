var gulp      = require('gulp'),
    less      = require('gulp-less'),
    watch     = require('gulp-watch'),
    minifyCSS = require('gulp-minify-css');

var paths = {
    less: ['./app/public/assets/less/app.less']
};

gulp.task('less', function () {
    gulp.src(paths.less[0])
        .pipe(less())
        .pipe(minifyCSS({keepBreaks:true}))
        .pipe(gulp.dest('./app/public/assets/css'))
});
gulp.task('watch', function() {
    gulp.watch(paths.less, ['less']);
});

gulp.task('default', ['watch']);