require.config({
    paths: {
        'domReady'         : '../bower_modules/requirejs-domready/domReady',
        'angular'          : '../bower_modules/angular/angular',
        'angular-animate'  : '../bower_modules/angular-animate/angular-animate',
        'angular-sanitize' : '../bower_modules/angular-sanitize/angular-sanitize',
        'angular-ui-router': '../bower_modules/angular-ui-router/release/angular-ui-router',
        'angular-bindonce' : '../bower_modules/angular-bindonce/bindonce',
        'angular-cookies'  : '../bower_modules/angular-cookies/angular-cookies',
        'angular-resource' : '../bower_modules/angular-resource/angular-resource',
        'angular-touch'    : '../bower_modules/angular-touch/angular-touch',
        'ngStorage'        : '../bower_modules/ngstorage/ngStorage',
        'lodash'           : '../bower_modules/lodash/dist/lodash',
        'restangular'      : '../bower_modules/restangular/dist/restangular',
        'angular-ui-utils' : 'modules/core/directives/ui-jq',
        'ui.load'          : 'modules/core/services/ui-load',
        'angular-bootstrap': '../bower_modules/angular-bootstrap/ui-bootstrap-tpls.min',
        'oclazyload'       : '../bower_modules/oclazyload/dist/ocLazyLoad',
        'angularjs-toaster': '../bower_modules/angularjs-toaster/toaster'
    },
    shim: {
        'angular'          : { exports: 'angular' },
        'angular-animate'  : { deps: ['angular'] },
        'angular-sanitize' : { deps: ['angular'] },
        'angular-ui-router': { deps: ['angular'] },
        'angular-bindonce' : { deps: ['angular'] },
        'angular-cookies'  : { deps: ['angular'] },
        'angular-resource' : { deps: ['angular'] },
        'angular-touch'    : { deps: ['angular'] },
        'ngStorage'        : { deps: ['angular'] },
        'lodash'           : { deps: ['angular'] },
        'restangular'      : { deps: ['angular','lodash'] },
        'angular-ui-utils' : { deps: ['angular'] },
        'ui.load'          : { deps: ['angular'] },
        'angular-bootstrap': { deps: ['angular']},
        'oclazyload'       : { deps: ['angular','angular-ui-utils']},
        'angularjs-toaster': { deps: ['angular']}
    },
    callback : function() {
        'use strict';

        require([
            'angular',
            'domReady',
            'app'
        ], function(angular, domReady) {
            domReady(function(){
                angular.bootstrap(document, ['app']);
            });
        });
    }
});