define([
    'angular',
    'angular-bindonce',
    'angular-animate',
    'angular-cookies',
    'angular-resource',
    'angular-sanitize',
    'angular-touch',
    'angular-ui-router',
    'ngStorage',
    'lodash',
    'restangular',
    'angular-ui-utils',
    'ui.load',
    'angular-bootstrap',
    'oclazyload',
    'angularjs-toaster',
    './modules/index'
], function(angular) {
    'use strict';

    var app = angular.module('app', [
        'pasvaz.bindonce',
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'ngStorage',
        'restangular',
        'ui.jq',
        'ui.load',
        'ui.bootstrap',
        'oc.lazyLoad',
        'toaster',
        'core.modules'
    ])
        .run(run)
        .config(configure)
        .constant('JQ_CONFIG', {
            easyPieChart:   ['/app/public/assets/js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
            sparkline:      ['/app/public/assets/js/jquery/charts/sparkline/jquery.sparkline.min.js'],
            plot:           ['/app/public/assets/js/jquery/charts/flot/jquery.flot.min.js',
                '/app/public/assets/js/jquery/charts/flot/jquery.flot.resize.js',
                '/app/public/assets/js/jquery/charts/flot/jquery.flot.tooltip.min.js',
                '/app/public/assets/js/jquery/charts/flot/jquery.flot.spline.js',
                '/app/public/assets/js/jquery/charts/flot/jquery.flot.orderBars.js',
                '/app/public/assets/js/jquery/charts/flot/jquery.flot.pie.min.js'],
            slimScroll:     ['/app/public/assets/js/jquery/slimscroll/jquery.slimscroll.min.js'],
            sortable:       ['/app/public/assets/js/jquery/sortable/jquery.sortable.js'],
            nestable:       ['/app/public/assets/js/jquery/nestable/jquery.nestable.js',
                '/app/public/assets/js/jquery/nestable/nestable.css'],
            filestyle:      ['/app/public/assets/js/jquery/file/bootstrap-filestyle.min.js'],
            slider:         ['/app/public/assets/js/jquery/slider/bootstrap-slider.js',
                '/app/public/assets/js/jquery/slider/slider.css'],
            chosen:         ['/app/public/assets/js/jquery/chosen/chosen.jquery.min.js',
                '/app/public/assets/js/jquery/chosen/chosen.css'],
            TouchSpin:      ['/app/public/assets/js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
                '/app/public/assets/js/jquery/spinner/jquery.bootstrap-touchspin.css'],
            wysiwyg:        ['/app/public/assets/js/jquery/wysiwyg/bootstrap-wysiwyg.js',
                '/app/public/assets/js/jquery/wysiwyg/jquery.hotkeys.js'],
            dataTable:      ['/app/public/assets/js/jquery/datatables/jquery.dataTables.min.js',
                '/app/public/assets/js/jquery/datatables/dataTables.bootstrap.js',
                '/app/public/assets/js/jquery/datatables/dataTables.bootstrap.css'],
            vectorMap:      ['/app/public/assets/js/jquery/jvectormap/jquery-jvectormap.min.js',
                '/app/public/assets/js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
                '/app/public/assets/js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
                '/app/public/assets/js/jquery/jvectormap/jquery-jvectormap.css'],
            footable:       ['/app/public/assets/js/jquery/footable/footable.all.min.js',
                '/app/public/assets/js/jquery/footable/footable.core.css']
        }
    );

    run.$inject = ['$rootScope', '$state', '$stateParams'];
    function run($rootScope, $state, $stateParams) {
        $rootScope._ = window._;
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
    }

    configure.$inject = ['$stateProvider', '$urlRouterProvider', 'RestangularProvider'];
    function configure($stateProvider, $urlRouterProvider, RestangularProvider) {
        $urlRouterProvider
            .otherwise('/dashboard');
        $stateProvider
            .state('app', {
                abstract: true,
                url: '',
                templateUrl: 'app/modules/core/views/app.html'
            });

        RestangularProvider.setBaseUrl('http://localhost:3000/api');
        RestangularProvider.setRestangularFields({
            id: "_id"
        });

    }

    return app;
});