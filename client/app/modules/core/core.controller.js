define(['./core.module'], function (core) {
    'use strict';

    core.controller('CoreCtrl', CoreCtrl);

    CoreCtrl.$inject = ['$scope', '$rootScope', '$localStorage'];
    function CoreCtrl($scope, $rootScope, $localStorage) {

        $scope.app = {
            name : 'ToDo',
            version : '0.0.1',
            color: {
                primary: '#7266ba',
                info:    '#23b7e5',
                success: '#27c24c',
                warning: '#fad733',
                danger:  '#f05050',
                light:   '#e8eff0',
                dark:    '#3a3f51',
                black:   '#1c2b36'
            },
            settings: {
                navbarHeaderColor: 'bg-black',
                navbarCollapseColor: 'bg-white-only',
                asideColor: 'bg-black',
                headerFixed: true,
                asideFixed: false,
                asideFolded: false,
                asideDock: false,
                container: false
            }
        };

        if ( angular.isDefined($localStorage.settings) ) {
            $scope.app.settings = $localStorage.settings;
        } else {
            $localStorage.settings = $scope.app.settings;
        }
        $scope.$watch('app.settings', function(){
            $localStorage.settings = $scope.app.settings;
        }, true);

    }
});