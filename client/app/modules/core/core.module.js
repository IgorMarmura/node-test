define(['angular'], function (angular) {
    'use strict';

    return angular.module('core.components', []);
});