define(['./projects.module'], function(module) {
    'use strict';

    module.controller('ProjectsController', ProjectsController);

    ProjectsController.$inject = ['$scope', 'toaster', 'projectsProvider', 'projects'];
    function ProjectsController($scope, toaster, projectsProvider, projects) {
        $scope.projects = projects;
        $scope.project = $scope.projects[0];
        $scope.projects[0].selected = true;
        $scope.project.editing = false;

        $scope.selectProject = selectProject;
        $scope.createProject = createProject;
        $scope.editProject   = editProject;
        $scope.removeProject = removeProject;
        $scope.saveProject   = saveProject;

        function selectProject(project) {
            angular.forEach($scope.projects, function(project) {
                project.selected = false;
            });
            $scope.project = project;
            $scope.project.selected = true;
        }

        function createProject() {
            $scope.project = {};
            $scope.project.editing = true;
        }

        function editProject() {
            $scope.project.editing = true;
        }

        function removeProject() {
            $scope.project
                .remove()
                .then(function(data) {
                    $scope.projects = _.remove($scope.projects, function(item){ return item !== $scope.project });
                    $scope.project = $scope.projects[0];
                    selectProject($scope.project);
                    toaster.pop('info', 'Оповещение', 'Проект успешно удален');
                }).catch(function(error) {
                    console.log('Fuck ' + error.data);
                });
        }

        function saveProject() {
            $scope.project.editing = false;
            if($scope.project._id) {
                $scope.project
                    .put()
                    .then(function(data) {
                        toaster.pop('info', 'Оповещение', 'Проект '+ data.name + ' успешно изменен');
                    }).catch(function(error) {
                        console.log('Fuck ' + error.data);
                    });
            }
            else {
                projectsProvider
                    .post($scope.project)
                    .then(function(data){
                        toaster.pop('info', 'Оповещение', 'Проект' + data.name + ' успешно создан');
                        $scope.project = data;
                        $scope.projects.push(data);
                        selectProject($scope.project);
                    })
                    .catch(function(error){
                        console.log('Fuck ' + error.data);
                    });
            }
        }

        $scope.d3 = [
            { label: "Closed tasks", data: 40 },
            { label: "Opened tasks", data: 10 },
            { label: "Tasks in work", data: 20 }
        ];
    }
});