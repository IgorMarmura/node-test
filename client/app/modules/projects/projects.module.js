define([
    'angular'
], function(angular) {
    "use strict";

    var projects = angular.module('core.projects', [
        'restangular'
    ])
        .config(configure);

    configure.$inject = ['$stateProvider'];
    function configure($stateProvider) {
        $stateProvider
            .state('app.projects', {
                url: '/projects',
                templateUrl: 'app/modules/projects/views/index.html',
                controller: 'ProjectsController',
                resolve: {
                    projectsProvider : getProjectsProvider,
                    projects : getProjects
                }
            });
    }

    getProjectsProvider.$inject = ['Restangular'];
    function getProjectsProvider(Restangular) {
        return Restangular.all('projects');
    }

    getProjects.$inject = ['Restangular'];
    function getProjects(Restangular) {
        return Restangular.all('projects').getList().then(function(data) {
            return data;
        });
    }



    return projects;
});