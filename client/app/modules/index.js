define([
    'angular',
    './core/index',
    './projects/index',
    './tasks/index',
    './dashboard/index'
], function(angular){
    'use strict';

    return angular.module('core.modules', [
        'core.components',
        'core.projects',
        'core.tasks',
        'core.dashboard'
    ]);
});