define([
    'angular'
], function(angular) {
    "use strict";

    var dashboard = angular.module('core.dashboard', [
        'restangular'
    ])
        .config(configure);

    configure.$inject = ['$stateProvider'];
    function configure($stateProvider) {
        $stateProvider
            .state('app.dashboard', {
                url: '/dashboard',
                templateUrl: 'app/modules/dashboard/views/index.html',
                controller: 'DashboardController'
            });
    }

    return dashboard;
});