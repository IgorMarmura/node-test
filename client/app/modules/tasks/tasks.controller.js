define(['./tasks.module'], function(module) {
    'use strict';

    module.controller('TasksController', TasksController);

    TasksController.$inject = ['$scope', 'toaster', 'tasksProvider', 'tasks'];
    function TasksController($scope, toaster, tasksProvider, tasks) {
        $scope.tasks = tasks;
        if(tasks.length) {
            $scope.task = $scope.tasks[0];
            $scope.tasks[0].selected = true;
            $scope.noData = false;
        }
        else {
            $scope.task = {};
            $scope.noData = true;
        }
        $scope.task.editing = false;

        $scope.selectTask = selectTask;
        $scope.createTask = createTask;
        $scope.editTask   = editTask;
        $scope.removeTask = removeTask;
        $scope.saveTask   = saveTask;

        function selectTask(task) {
            angular.forEach($scope.tasks, function(task) {
                task.selected = false;
            });
            $scope.task = task;
            $scope.task.selected = true;
        }

        function createTask() {
            $scope.task = {};
            $scope.task.editing = true;
            $scope.noData = false;
        }

        function editTask() {
            $scope.task.editing = true;
        }

        function removeTask() {
            $scope.task
                .remove()
                .then(function(data) {
                    $scope.tasks = _.remove($scope.tasks, function(item){ return item !== $scope.task });
                    $scope.task = $scope.tasks[0];
                    selectTasks($scope.task);
                    toaster.pop('info', 'Оповещение', 'Задача успешно удалена');
                }).catch(function(error) {
                    console.log('Fuck ' + error.data);
                });
        }

        function saveTask() {
            $scope.task.editing = false;
            if($scope.task._id) {
                $scope.task
                    .put()
                    .then(function(data) {
                        toaster.pop('info', 'Оповещение', 'Задача успешно изменен');
                    }).catch(function(error) {
                        console.log('Fuck ' + error.data);
                    });
            }
            else {
                tasksProvider
                    .post($scope.task)
                    .then(function(data){
                        toaster.pop('info', 'Оповещение', 'Задача успешно создан');
                        $scope.task = data;
                        $scope.tasks.push(data);
                        selectTask($scope.task);
                    })
                    .catch(function(error){
                        console.log('Fuck ' + error.data);
                    });
            }
        }

        $scope.d3 = [
            { label: "Closed tasks", data: 40 },
            { label: "Opened tasks", data: 10 },
            { label: "Tasks in work", data: 20 }
        ];
    }
});