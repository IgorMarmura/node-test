define([
    'angular'
], function(angular) {
    "use strict";

    var tasks = angular.module('core.tasks', [
        'restangular'
    ])
        .config(configure);

    configure.$inject = ['$stateProvider'];
    function configure($stateProvider) {
        $stateProvider
            .state('app.tasks', {
                url: '/tasks',
                templateUrl: 'app/modules/tasks/views/index.html',
                controller: 'TasksController',
                resolve: {
                    tasksProvider : getTasksProvider,
                    tasks : getTasks
                }
            });
    }

    getTasksProvider.$inject = ['Restangular'];
    function getTasksProvider(Restangular) {
        return Restangular.all('tasks');
    }

    getTasks.$inject = ['Restangular'];
    function getTasks(Restangular) {
        return Restangular.all('tasks').getList().then(function(data) {
            return data;
        });
    }

    return tasks;
});