# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Note for using sockets ###
Add to http.js following code

```
#!javascript

var io = require('socket.io').listen(8080);
io.sockets.on('connection', function (socket){
    console.info('user connected - ' + socket.id);

    socket.on('test', function (data) {
        console.log(data);
    });
    socket.emit('init', {
        name: 'Azza',
        users: 'testusers'
    });
});
```
