var express = require('express'),
    config  = require('./components/config'),
    mongoose  = require('./components/mongoose'),
    path    = require('path'),
    parser  = require('body-parser'),
    log     = require('./components/log')(module),
    baucis  = require('baucis'),
    cors    = require('express-cors'),
    app     = express();

app.use(cors({
    allowedOrigins: [
        'todo.loc'
    ]
}));

app.use(parser());
app.use('/api', baucis());

app.listen(config.get('port'), function(){
    log.info('Express server listening on port ' + config.get('port'));
    log.info('Environment:' + config.get('NODE_ENV'))
});