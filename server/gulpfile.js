var gulp      = require('gulp'),
    nodemon   = require('gulp-nodemon'),
    jshint    = require('gulp-jshint'),
    less      = require('gulp-less'),
    minifyCSS = require('gulp-minify-css');

gulp.task('default', ['run']);

gulp.task('lint', function() {
    gulp.src('./**/*.js')
        .pipe(jshint());
});
gulp.task('less', function () {
    gulp.src('./public/less/main.less')
        .pipe(less())
        //.pipe(minifyCSS({keepBreaks:true}))
        .pipe(gulp.dest('./public/css'))
});
gulp.task('run', function() {
    nodemon({ script: 'server.js' })
        .on('change', ['lint'])
        .on('restart', function() {
            console.log('Server restarted!');
        });
});