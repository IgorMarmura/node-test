var companies = {};
companies.name = 'companies';
companies.methods = 'put get';
companies.chema = {
    name             : { type: String, default: null },
    domain           : { type: String, default: null },
    description      : { type: String, default: null },
    projects         : { type: [], default: null},
    created_at       : { type: Date, default: Date.now },
    created_by	     : { type: Number, default: null, index: true },
    deleted_at       : { type: Date, default: Date.now }
};
module.exports = companies;