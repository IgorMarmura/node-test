var projects = {};
projects.name = 'projects';
projects.methods = '';
projects.chema = {
    name        : { type: String, default: null },
    description : { type: String, default: null },
    users       : { type: [], default: null },
    created_at  : { type: Date, default: Date.now },
    created_by	: { type: Number, default: null, index: true }
};
module.exports = projects;