var tasks = {};
tasks.name = 'tasks';
tasks.methods = '';
tasks.chema = {
    user_id   	  	   : { type: Number, default: null, index: true },
    project_id  	   : { type: Number, default: null, index: true },
    checker_id  	   : { type: Number, default: null, index: true },
    title	    	   : { type: String, default: null },
    description 	   : { type: String, default: null },
    time_limit  	   : { type: Number, default: null },
    spend_time  	   : { type: Number, default: null },
    completed_percent  : { type: Number, default: null },
    task_priority_id   : { type: Number, default: null, index: true },
    created_at  	   : { type: Date, default: Date.now },
    created_by		   : { type: Number, default: null, index: true }
};
module.exports = tasks;