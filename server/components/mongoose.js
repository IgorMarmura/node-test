var mongoose    = require('mongoose'),
    fs          = require('fs'),
    path        = require('path'),
    baucis      = require('baucis'),
    log         = require('./log')(module);

mongoose.connect('mongodb://localhost/todo');
var db = mongoose.connection;

db.on('error', function (err) {
    log.error('connection error:', err.message);
});
db.once('open', function callback () {
    log.info("Connected to mongo!");
});

var models = fs.readdirSync('./models');
models.forEach(function(model) {
    var name = path.basename(model, '.js');
    var item = require('./../models/' + model);
    name = baucis.rest(mongoose.model(name, new mongoose.Schema(item.chema)));
    name.methods(item.methods, false);
});